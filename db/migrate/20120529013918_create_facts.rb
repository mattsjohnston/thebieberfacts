class CreateFacts < ActiveRecord::Migration
  def change
    create_table :facts do |t|
      t.integer :category_id
      t.text :fact
      t.string :slug
      t.integer :votes

      t.timestamps
    end
  end
end
