class Category < ActiveRecord::Base
  attr_accessible :name, :slug
end
